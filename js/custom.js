function show_top_button(){
	if ( ($(window).width() >= 1400) && ($(window).scrollTop() >= 500) ){
		$('.to-top-button').each(function(){
			$(this).fadeIn(300);
		})
	} else {
		$('.to-top-button').each(function(){
			$(this).fadeOut(200);
		})
	}
}
function items_slider() {
	console.log('slider restarted!');
	$('.slider--type-shop').each(function(){
		var wrapper = $(this);
		if ( wrapper.hasClass('slick-initialized') ) {
			wrapper.slick('unslick');
			$('.slick-list', wrapper).remove();
		}
		wrapper.slick({
			prevArrow: '<div class="slider__button slider__button--dark slider__button--prev"></div>',
			nextArrow: '<div class="slider__button slider__button--dark slider__button--next"></div>',
			mobileFirst: true,
			zIndex: 1,
			arrows: false,
			slidesToScroll: 1,
			dots: false,
			slidesToShow: 5,
			infinite: false,
			responsive: [
				{
					breakpoint: 1499,
					settings: {
						arrows: true,
						dots: false,
						slidesToShow: 5
					}
				},
				{
					breakpoint: 1199,
					settings: {
						arrows: false,
						dots: false,
						slidesToShow: 5
					}
				},
				{
					breakpoint: 1023,
					settings: {
						arrows: false,
						dots: false,
						slidesToShow: 4
					}
				},
				{
					breakpoint: 699,
					settings: {
						arrows: false,
						dots: false,
						slidesToShow: 3
					}
				},
				{
					breakpoint: 599,
					settings: {
						arrows: false,
						dots: false,
						slidesToShow: 2
					}
				},
				{
					breakpoint: 319,
					settings: 'unslick'
				}
			]
		});
	});
}
function news_slider() {
	var wrapper = $('.slider--type-news');
	if ( wrapper.hasClass('slick-initialized') ) {
		wrapper.slick('unslick');
	}
	wrapper.slick({
		prevArrow: '<div class="slider__button slider__button--dark slider__button--prev"></div>',
		nextArrow: '<div class="slider__button slider__button--dark slider__button--next"></div>',
		mobileFirst: true,
		zIndex: 1,
		arrows: false,
		slidesToScroll: 1,
		dots: false,
		slidesToShow: 4,
		responsive: [
			{
				breakpoint: 1499,
				settings: {
					arrows: true,
					slidesToShow: 4
				}
			},
			{
				breakpoint: 1199,
				settings: {
					mobileFirst: true,
					zIndex: 1,
					arrows: false,
					slidesToScroll: 1,
					dots: false,
					slidesToShow: 4
				}
			},
			{
				breakpoint: 959,
				settings: {
					mobileFirst: true,
					zIndex: 1,
					arrows: false,
					slidesToScroll: 1,
					dots: false,
					slidesToShow: 3
				}
			},
			{
				breakpoint: 599,
				settings: {
					mobileFirst: true,
					zIndex: 1,
					arrows: false,
					slidesToScroll: 1,
					dots: false,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 319,
				settings: 'unslick'
			}
		]
	});
}
function gallery_slider() {
	var wrapper = $('.slider--type-gallery'),
		wrapper2 = $('.slider--type-thumbs');
	if ( wrapper.hasClass('slick-initialized') ) {
		wrapper.slick('unslick');
	}
	if ( wrapper2.hasClass('slick-initialized') ) {
		wrapper2.slick('unslick');
	}
	wrapper2.each(function(){
		$(this).slick({
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 4,
			slidesToScroll: 1,
			adaptiveHeight: false,
			dots: false,
			arrows: true,
			asNavFor: $('.slider--type-gallery', $(this).parents('.wrapper-gallery')),
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 1023,
					settings: {
						prevArrow: '<div class="slider__button slider__button--light slider__button--prev"></div>',
						nextArrow: '<div class="slider__button slider__button--light slider__button--next"></div>',
						vertical: true,
						verticalSwiping: true,
						slidesToShow: 5,
						swipeToSlide: false
					}
				},
				{
					breakpoint: 699,
					settings: {
						prevArrow: '<div class="slider__button slider__button--light slider__button--prev"></div>',
						nextArrow: '<div class="slider__button slider__button--light slider__button--next"></div>',
						vertical: false,
						verticalSwiping: false,
						slidesToShow: 4
					}
				},
				{
					breakpoint: 319,
					settings: {
						prevArrow: '<div class="slider__button slider__button--light slider__button--prev"></div>',
						nextArrow: '<div class="slider__button slider__button--light slider__button--next"></div>',
						vertical: false,
						verticalSwiping: false,
						slidesToShow: 3
					}
				}
			]
		});
	});
	wrapper.each(function(){
		$(this).slick({
			customPaging: function(){
				return ''
			},
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: true,
			dots: false,
			arrows: false,
			fade: true,
			draggable: true,
			swipe: true,
			touchMove: true,
			responsive: [
				{
					breakpoint: 759,
					settings: {
						dots: false,
						draggable: false,
						swipe: false,
						touchMove: false
					}
				},
				{
					breakpoint: 319,
					settings: {
						dots: true
					}
				}
			]
		});
	});
}


function reselect(wrapper) {
	$('.select', wrapper).each(function(){
		let i = 0;
		$(this).val('').after('<div class="styled-select__select"></div><ul class="styled-select__option-list"></ul>');
		$('option', this).each(function(){
			if ( ($(this).attr('selected') == 'selected') || ($(this).prop('selected') == true)) {
				$(this).parents('.select').siblings('.styled-select__option-list').append('<li class="styled-select__option styled-select__option--state-active">'+$(this).html()+'</li>');
				$('.styled-select__select', $(this).parents('.styled-select')).html($(this).html());
			} else {
				$(this).parents('.select').siblings('.styled-select__option-list').append('<li class="styled-select__option">'+$(this).html()+'</li>');
			}
		});
		$(this).css({'position': 'absolute', 'left': '-9999px', 'visibility': 'hidden'}).parents('.styled-select').css('z-index', (100-i));
		i++;
	});
	wrapper.find('.styled-select').off().click( function(){
		//wrapper.on('click', '.styled-select', function(){
		if ($(this).hasClass('disabled')) {
			return false;
		} else {
			$(this).toggleClass('styled-select--state-active');
			let obj = $(this);
			$(document).click(function(event) {
				if ($(event.target).closest(obj).length) return;
				obj.removeClass('styled-select--state-active');
				event.stopPropagation();
			});
		}
	});
	wrapper.find('.styled-select__option').off().click( function(){
		//wrapper.on('click', '.styled-select__option', function(){
		let index = $(this).index();
		$(this).addClass('styled-select__option--state-active').siblings('.styled-select__option').removeClass('styled-select__option--state-active');
		$('.styled-select__select', $(this).parents('.styled-select')).html($(this).html());
		$('.select option', $(this).closest('.styled-select')).removeAttr('selected').prop('selected', false);
		$('.select option', $(this).closest('.styled-select')).eq(index).prop('selected', true).attr('selected', 'selected');
		$('.select', $(this).closest('.styled-select')).val($('option:selected', $(this).parents('.styled-select')).val()).change().keyup();
	});
}

$(document).ready(function(){

// Slider
	$('.slider--type-main').each(function(){
		$(this).slick({
			prevArrow: '<div class="slider__button slider__button--light slider__button--prev"></div>',
			nextArrow: '<div class="slider__button slider__button--light slider__button--next"></div>',
			customPaging: function(){
				return ''
			},
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 1,
			adaptiveHeight: true,
			dots: true,
			arrows: false,
			slidesToScroll: 1,
			fade: true,
			responsive: [
				{
					breakpoint: 1023,
					settings: {
						arrows: true
					}
				},
				{
					breakpoint: 319,
					settings: {
						arrows: false
					}
				}
			]
		});
	});
	$('.slider--type-banner').each(function(){
		$(this).slick({
			customPaging: function(){
				return ''
			},
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 1,
			adaptiveHeight: true,
			dots: true,
			arrows: false,
			slidesToScroll: 1,
			fade: true
		});
	});
	$('.slider--type-viewed').each(function(){
		$(this).slick({
			prevArrow: '<div class="slider__button slider__button--vertical slider__button--prev"></div>',
			nextArrow: '<div class="slider__button slider__button--vertical slider__button--next"></div>',
			mobileFirst: true,
			zIndex: 1,
			slidesToShow: 6,
			adaptiveHeight: true,
			dots: false,
			arrows: true,
			slidesToScroll: 1,
			vertical: true,
			verticalSwiping: true
		});
	});
	gallery_slider();
	items_slider();
	news_slider();
	$(window).resize(function(){
		items_slider();
		news_slider();
	});

// Mobile menu
	$('.header__mobile-catalog-button').on('click', function(){
		$(this).toggleClass('header__mobile-catalog-button--state-active');
		$('.mobile-menu').fadeToggle(300);
		if ($(this).hasClass('header__mobile-catalog-button--state-active')) {
			var path = [],
				i = 0;
			path[0] = $('.mobile-menu').html();
			$('.mobile-menu').on('click', '.mobile-step', function(){
				var title = $(this).children('.mobile-menu__title').text();
				var value = $(this).children('.mobile-content').html();
				i++;
				if ($(this).hasClass('mobile-step--no-title')) {
					path[i] = '<span class="mobile-menu__item mobile-menu__back">' +
						'<span class="mobile-menu__title">&lt; РќР°Р·Р°Рґ</span>' +
						'</span>' +
						value;
				} else {
					path[i] = '<span class="mobile-menu__item mobile-menu__back">' +
						'<span class="mobile-menu__title">&lt; РќР°Р·Р°Рґ</span>' +
						'</span>' +
						'<div class="mobile-menu__heading">' + title + '</div>' +
						value;
				}
				$('.mobile-menu').html(path[i]);
			});
			$('.mobile-menu').on('click', '.mobile-menu__back', function(){
				i--;
				$('.mobile-menu').html(path[i]);
			});
		} else {
			path = [];
			i = 0;
		}
	});

// Pop-up window
	$('.fancybox').fancybox({
		padding: 0,
		margin: 10
	});
	$('.fancybox-window').fancybox({
		padding: 0,
		margin: 20,
		afterShow: function(current){
			gallery_slider();
		}
	});

// Category spoiler
	$('.block-category__title').on('click', function(){
		if ($(window).width() < 600) {
			$(this).toggleClass('block-category__title--state-active').siblings('.block-category__content').slideToggle(300);
		} else {
			$(this).removeClass('block-category__title--state-active').siblings('.block-category__content').removeAttr('style');
		}
	});

// Brand spoiler
	$('.block-brand-card__more').on('click', function(){
		var beforeVal = $(this).text(),
			afterVal = $(this).attr('data-active');
		$(this).text(afterVal).attr('data-active', beforeVal).toggleClass('block-brand-card__more--state-active');
		$('.block-brand-card__hidden', $(this).parents('.block-brand-card')).slideToggle(300);
	});

// Filter spoiler
	$('.block-filter__link').on('click', function(){
		$(this).toggleClass('block-filter__link--state-active');
		$('.block-filter__content', $(this).parents('.block-filter')).slideToggle(300);
	});

// FAQ spoiler
	$('.block-faq__link').on('click', function(){
		$(this).toggleClass('block-faq__link--state-active');
		$('.block-faq__content', $(this).parents('.block-faq')).slideToggle(300);
	});

// Additional spoiler
	$('.wrapper-additional__more-link').on('click', function(){
		$(this).toggleClass('wrapper-additional__more-link--state-active');
		$('.wrapper-additional__hidden', $(this).parents('.wrapper-additional__spoiler')).slideToggle(300);
	});

// Styled checkboxes
	$('.input--type-checkbox').each(function(){
		$(this).children('input:checkbox').css('left','-9999px');
		$(this).children('input:checkbox').after('<div class="checkbox"></div>');
		if ($(this).children('input:checkbox').prop('checked')==true) {
			$(this).addClass('checked');
		}
		if ($(this).children('input:checkbox').prop('disabled')==true) {
			$(this).addClass('disabled');
		}
	});
	$('.input--type-checkbox .checkbox, .input--type-checkbox .input__label').on('click', function(){
		$(this).parents('.input--type-checkbox').toggleClass('checked');
		if ($(this).parents('.input--type-checkbox').hasClass('checked')){
			$('input:checkbox', $(this).parents('.input--type-checkbox')).attr('checked','checked').prop('checked', true).change();
		} else {
			$('input:checkbox', $(this).parents('.input--type-checkbox')).removeAttr('checked').prop('checked', false).change();
		}
	});

// Styled radio
	$('.input--type-radio').each(function(){
		$(this).children('input:radio').css('left','-9999px');
		$(this).children('input:radio').after('<div class="radio"></div>');
		if ($(this).children('input:radio').prop('checked')==true) {
			$(this).addClass('checked');
		}
		if ($(this).children('input:radio').prop('disabled')==true) {
			$(this).addClass('disabled');
		}
	});
	$('.input--type-radio .radio, .input--type-radio .input__label').on('click', function(){
		$('.input--type-radio', $(this).parents('.radio-set')).removeClass('checked');
		$('input:radio', $(this).parents('.radio-set')).removeAttr('checked').prop('checked', false).change();
		$(this).parents('.input--type-radio').addClass('checked');
		$('input:radio', $(this).parents('.input--type-radio')).attr('checked','checked').prop('checked', true).change();
	});

// Estimate
	$('.block-estimate__value').on('click', function(){
		$(this).siblings('.block-estimate__value').removeClass('block-estimate__value--state-active').children('input:radio').removeAttr('checked').prop('checked', false).change();
		$(this).addClass('block-estimate__value--state-active').children('input:radio').attr('checked','checked').prop('checked', true).change();
	});

// Styled file
	$('.input--type-file input:file').each(function(){
		if ($(this).val()!='') {
			$('.file', $(this).parents('.input--type-file')).text($(this).val().replace(/^.*[\\\/]/, ''));
		}
	});
	$('.input--type-file input:file').on('change', function(){
		$('.file', $(this).parents('.input--type-file')).text($(this).val().replace(/^.*[\\\/]/, ''));
	});

// Simple tabs
	$('.block-tabs').each(function(){
		$('.tab', this).eq($('.block-tabs__control .block-tabs__control-item--state-active', this).index()).addClass('tab--state-active');
	});
	$('.block-tabs .block-tabs__control-item').on('click', function(){
		if ($(this).hasClass('block-tabs__control-item--state-active')){
			return false;
		} else {
			$(this).addClass('block-tabs__control-item--state-active').siblings('.block-tabs__control-item').removeClass('block-tabs__control-item--state-active');
			$('.tab', $(this).parents('.block-tabs')).removeClass('tab--state-active').eq($(this).index()).addClass('tab--state-active');
			items_slider();
		}
	});

// Switcher
	$('.js-switcher__button').each(function(){
		if ($(this).hasClass('checked')) {
			$('.js-switcher__content', $(this).parents('.js-switcher')).slideDown(300);
		} else {
			$('.js-switcher__content', $(this).parents('.js-switcher')).slideUp(300);
		}
	});
	$('.js-switcher input:checkbox, .js-switcher input:radio').on('change', function(){
		if ($('.js-switcher__button', $(this).parents('.js-switcher')).hasClass('checked')) {
			$('.js-switcher__content', $(this).parents('.js-switcher')).slideDown(300);
		} else {
			$('.js-switcher__content', $(this).parents('.js-switcher')).slideUp(300);
		}
	});

// Mobile sort
	$('.block-sort-mobile__button').on('click', function(){
		$('.block-sort-mobile__content', $(this).parents('.block-sort-mobile__item')).slideToggle(300);
	});

// Styled select
	var i = 0;
	// $('.select').each(function(){
	//     $(this).val('').after('<div class="styled-select__select"></div><ul class="styled-select__option-list"></ul>');
	//     $('option', this).each(function(){
	// 	if ( ($(this).attr('selected') == 'selected') || ($(this).prop('selected') == true)) {
	// 		$(this).parents('.select').siblings('.styled-select__option-list').append('<li class="styled-select__option styled-select__option--state-active">'+$(this).html()+'</li>');
	// 		$('.styled-select__select', $(this).parents('.styled-select')).html($(this).html());
	// 	} else {
	// 		$(this).parents('.select').siblings('.styled-select__option-list').append('<li class="styled-select__option">'+$(this).html()+'</li>');
	// 	}
	//         // $(this).removeAttr('selected').prop('selected', false).parents('.select').siblings('.styled-select__option-list').append('<li class="styled-select__option">'+$(this).html()+'</li>');
	//     });
	//    // $(this).siblings('.styled-select__select').html($('option:first', this).html());
	//     $(this).css({'position': 'absolute', 'left': '-9999px', 'visibility': 'hidden'}).parents('.styled-select').css('z-index', (100-i));
	//     i++;
	// });
	// $('.styled-select').on('click', function(){
	//     if ($(this).hasClass('disabled')) {
	//         return false;
	//     } else {
	//         $(this).toggleClass('styled-select--state-active');
	//         var obj = $(this);
	//         $(document).click(function(event) {
	//             if ($(event.target).closest(obj).length) return;
	//             obj.removeClass('styled-select--state-active');
	//             event.stopPropagation();
	//         });
	//     }
	// });
	// $('.styled-select .styled-select__option').bind('click', function(){
	//     var index = $(this).index();
	//     $(this).addClass('styled-select__option--state-active').siblings('.styled-select__option').removeClass('styled-select__option--state-active');
	//     $('.styled-select__select', $(this).parents('.styled-select')).html($(this).html());
	//     $('.select option', $(this).closest('.styled-select')).removeAttr('selected').prop('selected', false);
	//     $('.select option', $(this).closest('.styled-select')).eq(index).prop('selected', true).attr('selected', 'selected');
	//     $('.select', $(this).closest('.styled-select')).val($('option:selected', $(this).parents('.styled-select')).val()).change().keyup();
	// });

	reselect($('.container'));

// Sticky menu
// 	var headerTop = $('.header').height()*2;
// 	sticky_menu(headerTop);
// 	$(window).on('resize', function () {
// 		headerTop = $('.header').height()*2;
//         sticky_menu(headerTop);
// 	});
// 	$(window).on('scroll', function(){
// 		sticky_menu(headerTop);
// 	});

// Filters spoiler
// 	$('.filters__more').on('click', function(){
// 		$(this).parents('.filters').toggleClass('filters--state-active');
// 	});


// Custom scrollbar
// 	$('.block-table__body').each(function(){
// 		$(this).jScrollPane({
// 			verticalGutter: 11,
// 			mouseWheelSpeed: 50
// 		});
// 	});

// Styled number
	$('.input__button--plus').on('click', function(){
		var numValue = parseInt($('.input__number', $(this).parents('.input--type-number')).val());
		$('.input__number', $(this).parents('.input--type-number')).val(numValue + 1);
	});
	$('.input__button--minus').on('click', function(){
		var numValue = parseInt($('.input__number', $(this).parents('.input--type-number')).val());
		if (numValue > 1){
			$('.input__number', $(this).parents('.input--type-number')).val(numValue - 1);
		}
	});

// To top button
	show_top_button();
	$(window).on('scroll', function(){
		show_top_button();
	});
	$(window).on('resize', function(){
		show_top_button();
	});

// Auto scroll
	$('.js-scroller').on('click', function(){
		var target = $(this).attr('href');
		$('body, html').animate({
			scrollTop: $(target).offset().top + 'px'
		}, 600);
		return false;
	});

// Desktop catalog
	$('.block-catalog__title').on('click', function(){
		$(this).toggleClass('block-catalog__title--state-active');
		var obj = $(this).parents('.block-catalog');
		$('.block-catalog__content', obj).fadeToggle(300);
		$(document).click(function(event) {
			if ($(event.target).closest(obj).length) return;
			$('.block-catalog__content', obj).fadeOut(300);
			$('.block-catalog__title', obj).removeClass('block-catalog__title--state-active');
			event.stopPropagation();
		});
	});
	$('.block-catalog__menu-item.has-children').on('mouseenter', function(){
		var obj = $(this);
		$('.block-catalog__submenu', obj).fadeIn(300);
		obj.parents('.block-catalog__menu').height($('.block-catalog__submenu', obj).outerHeight());
	});
	$('.block-catalog__menu-item.has-children').on('mouseleave', function(){
		var obj = $(this);
		$('.block-catalog__submenu', obj).fadeOut(300);
		obj.parents('.block-catalog__menu').removeAttr('style');
	});
	$('.block-catalog__submenu-item.has-children').on('mouseenter', function(){
		var obj = $(this);
		$('.block-catalog__internalmenu', obj).fadeIn(300);
	});
	$('.block-catalog__submenu-item.has-children').on('mouseleave', function(){
		var obj = $(this);
		$('.block-catalog__internalmenu', obj).fadeOut(300);
	});

// Datepicker
	$('.input__datepicker').each(function() {
		$(this).datepicker({
			showOn: 'both',
			prevText: '<',
			nextText: '>',
			closeText: 'РџРѕРєР°Р·Р°С‚СЊ',
			currentText: 'РЎРµРіРѕРґРЅСЏ',
			monthNames: ['РЇРЅРІР°СЂСЊ', 'Р¤РµРІСЂР°Р»СЊ', 'РњР°СЂС‚', 'РђРїСЂРµР»СЊ', 'РњР°Р№', 'РСЋРЅСЊ', 'РСЋР»СЊ', 'РђРІРіСѓСЃС‚', 'РЎРµРЅС‚СЏР±СЂСЊ', 'РћРєС‚СЏР±СЂСЊ', 'РќРѕСЏР±СЂСЊ', 'Р”РµРєР°Р±СЂСЊ'],
			monthNamesShort: ['РЇРЅРІР°СЂСЏ', 'Р¤РµРІСЂР°Р»СЏ', 'РњР°СЂС‚Р°', 'РђРїСЂРµР»СЏ', 'РњР°СЏ', 'РСЋРЅСЏ', 'РСЋР»СЏ', 'РђРІРіСѓСЃС‚Р°', 'РЎРµРЅС‚СЏР±СЂСЏ', 'РћРєС‚СЏР±СЂСЏ', 'РќРѕСЏР±СЂСЏ', 'Р”РµРєР°Р±СЂСЏ'],
			dayNames: ['РІРѕСЃРєСЂРµСЃРµРЅСЊРµ', 'РїРѕРЅРµРґРµР»СЊРЅРёРє', 'РІС‚РѕСЂРЅРёРє', 'СЃСЂРµРґР°', 'С‡РµС‚РІРµСЂРі', 'РїСЏС‚РЅРёС†Р°', 'СЃСѓР±Р±РѕС‚Р°'],
			dayNamesShort: ['РІСЃРє', 'РїРЅРґ', 'РІС‚СЂ', 'СЃСЂРґ', 'С‡С‚РІ', 'РїС‚РЅ', 'СЃР±С‚'],
			dayNamesMin: ['Р’СЃ', 'РџРЅ', 'Р’С‚', 'РЎСЂ', 'Р§С‚', 'РџС‚', 'РЎР±'],
			weekHeader: 'РќРµРґ',
			dateFormat: 'dd M yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: '',
			buttonText: '',
			minDate: 1
		})
	});

// Close filter
	$('.block-active-filters__close').on('click', function () {
		$(this).parents('.block-active-filters__item').remove();
	});

// Tooltips
	$('.tooltip__title').on('click', function(){
		$('.tooltip__content', $(this).parents('.tooltip')).fadeToggle(300);
		var obj = $(this).parents('.tooltip');
		$(document).click(function(event) {
			if ($(event.target).closest(obj).length) return;
			$('.tooltip__content', obj).fadeOut(200);
			event.stopPropagation();
		});
	});
	$('.tooltip__close').on('click', function(){
		$('.tooltip__content', $(this).parents('.tooltip')).fadeOut(200);
	});

// Dropdown menu
	$('.block-dropdown__title-link').on('click', function(){
		$(this).parents('.block-dropdown').toggleClass('block-dropdown--state-active');
		$('.block-dropdown__list', $(this).parents('.block-dropdown')).slideToggle(300);
		var obj = $(this).parents('.block-dropdown');
		$(document).click(function(event) {
			if ($(event.target).closest(obj).length) return;
			obj.removeClass('block-dropdown--state-active');
			$('.block-dropdown__list', obj).slideUp(200);
			event.stopPropagation();
		});
	});

});